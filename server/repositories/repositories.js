const users = require('./userlist');
const texts = require('./textlist');

class Data {
    static getUser(request) {
        return users.find(userFromDB => {
            if (userFromDB.login === request.login) {
                return userFromDB;
            }            
        });
    }

    static getText(id) {
        return texts.find(textFromDB => {
            if (textFromDB.id === id) {
                return textFromDB;
            }            
        });
    }
}  

module.exports = {
    Data
}