/* eslint-disable no-else-return */
/* eslint-disable global-require */
/* eslint-disable no-plusplus */
/* eslint-disable no-console */
/* eslint-disable no-param-reassign */
/* eslint-disable import/no-dynamic-require */
const path = require('path');
const express = require('express');

const app = express();
const server = require('http').Server(app);
const passport = require('passport');
const bodyParser = require('body-parser');
// const moment = require('moment');
const io = require('socket.io')(server);

const loginRoutes = require('./server/routes/login');
const gameRoutes = require('./server/routes/game');
const gameSocket = require('./server/socket/socket.game');

require(path.join(__dirname, 'server', 'passport.config'));
app.use(passport.initialize());

app.use(express.static(path.join(__dirname, 'client')));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'client', 'index.html'));
});


app.use('/', loginRoutes);
app.use('/', gameRoutes);

io.on('connection', (socket) => {
  //  Proxy pattern
  socket = new Proxy(socket, {
    get(target, prop) {
      if (prop in target) {
        return target[prop];
      } else {
        console.log(`No socket prop: ${prop}`);
        target[prop] = '';
        return target[prop];
      }
    },
  });

  gameSocket({
    io,
    socket,
  });
});

server.listen(3000);
