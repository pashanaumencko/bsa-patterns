/* eslint-disable max-len */
const curry = require('lodash/fp/curry');


//  Currying implementation using LoDash
const getRandomIntNumber = curry((leftLimit, rightLimit) => Math.floor(Math.random() * (rightLimit - leftLimit + 1) + leftLimit));

module.exports = getRandomIntNumber;
