const geRandomNumber = require('./helper');

module.exports = {
  initiatedAt: null,
  isInitiated: false,
  results: [],
  userProgress: {},
  textId: geRandomNumber(1)(4),
};
