/* eslint-disable no-param-reassign */
/* eslint-disable no-console */
/* eslint-disable object-curly-newline */
/* eslint-disable class-methods-use-this */

class Bot {
  constructor(io, socket) {
    this.io = io;
    this.socket = socket;
  }

  getStartMessage() {
    const message = `На улице сейчас немного пасмурно, 
    но на Львов Арена сейчас просто замечательная атмосфера:
    двигатели рычат, зрители улыбаются а гонщики едва заметно 
    нервничают и готовят своих железных коней до заезда. А комментировать 
    это все действо Вам буду я, Эскейп Ентерович и я рад вас приветствовать со словами Доброго Вам дня господа!`;

    return message;
  }

  getListWithIntroducing(userProgress) {
    return Object.keys(userProgress).map((player, index) => `${player} под номером ${index + 1}`);
  }

  createIntroduceMessage(userProgress) {
    let message = 'А тем временем, список гонщиков: ';
    const players = this.getListWithIntroducing(userProgress);
    message += players.join(', ');

    return message;
  }

  sortUserByProgress(userProgress) {
    return Object.keys(userProgress).sort((player, anotherPlayer) => {
      console.log(`${userProgress[player].progress} while sorting`);
      const { progress: playerProgress } = userProgress[player];
      const { progress: anotherPlayerProgress } = userProgress[anotherPlayer];
      return anotherPlayerProgress - playerProgress;
    });
  }

  createMessageAboutLeaders(userProgress) {
    const message = this.sortUserByProgress(userProgress).map((player, index) => {
      switch (index) {
        case 0:
          return `Cейчас первый ${player} с результатом в ${userProgress[player].progress} символов`;
        case 1:
          return `вторым идет ${player} с результатом в ${userProgress[player].progress} символов`;
        case 2:
          return `третьим идет ${player} с результатом в ${userProgress[player].progress} символов`;
        default:
          return `потом идет ${player} с результатом в ${userProgress[player].progress} символов`;
      }
    });

    const createdMessage = message.join(', ');
    return createdMessage;
  }

  createConcludionMessage(userProgress) {
    const currentPlayerRating = this.sortUserByProgress(userProgress);
    const message = `До финиша осталось совсем немного и похоже что первым его может пересечь ${currentPlayerRating[0]}. 
    Второе место может остаться ${currentPlayerRating[1]} или ${currentPlayerRating[2]}. Но давайте дождемся финиша.`;

    return message;
  }

  createPlayerFinishedMessage(userLogin) {
    const message = `${userLogin} финишировал`;

    return message;
  }

  createAllPlayersFinishedMessage() {
    const message = 'Все гонщики финишировали';

    return message;
  }
}

module.exports = Bot;
