const jwt = require('jsonwebtoken');
const path = require('path');
const express = require('express');
const repo = require(path.join(__dirname, '..', 'repositories', 'repositories.js'));
const router = express.Router();
  

router.get('/login', function (req, res) {
    res.sendFile(path.join(__dirname, '..', '..', 'client', 'login.html'));
});
  
router.post('/login', function (req, res) {
    const userFromReq = req.body;
    const userInDB = repo.Data.getUser(userFromReq);
    if (userInDB && userInDB.password === userFromReq.password) {
      const token = jwt.sign(userFromReq, 'someSecret');
      res.status(200).json({ auth: true, token });
    } else {
      res.status(401).json({ auth: false, message: 'Incorrect login or password' });
    }
});

module.exports = router;