const { KindJokeGenerator, EvilJokeGenerator, FactGenerator } = require('./random.message');
const geRandomNumber = require('./helper');

//  Facade implementation
class LaunchBot {
  constructor(botInstance) {
    this.botInstance = botInstance;
  }

  initiateBot() {
    const message = this.botInstance.getStartMessage();

    if (this.botInstance.socket.room === 'gameRoom') {
      this.botInstance.socket.emit('greetingsMesssage', { message });
    }
  }

  introducePlayers(userProgress) {
    const message = this.botInstance.createIntroduceMessage(userProgress);
    this.botInstance.socket.emit('introducePlayers', { message });
  }

  informAboutLeaders(userProgress) {
    const message = this.botInstance.createMessageAboutLeaders(userProgress);
    this.botInstance.socket.emit('informAboutLeaders', { message });
  }

  sayRandomMessage() {
    const messages = [
      new KindJokeGenerator(this.botInstance.socket),
      new EvilJokeGenerator(this.botInstance.socket),
      new FactGenerator(this.botInstance.socket),
    ];
    const numberOfMessage = geRandomNumber(0)(2);

    messages[numberOfMessage].getTypeOfMessage();
  }

  informAboutConcludion(userProgress) {
    const message = this.botInstance.createConcludionMessage(userProgress);
    this.botInstance.socket.emit('informAboutConcludion', { message });
  }

  informAboutPlayerFinishing(userLogin, userProgress) {
    const isPlayerFinish = userProgress[userLogin].progress === userProgress[userLogin].textLength;

    if (isPlayerFinish) {
      const message = this.botInstance.createPlayerFinishedMessage(userLogin);
      this.botInstance.socket.emit('informAboutPlayerFinishing', { message });
    }
  }

  informAboutAllPlayersFinishing() {
    const message = this.botInstance.createAllPlayersFinishedMessage();
    this.botInstance.socket.emit('informAboutAllPlayersFinishing', { message });
  }
}

module.exports = LaunchBot;
