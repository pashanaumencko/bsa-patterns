/* eslint-disable class-methods-use-this */
const geRandomNumber = require('./helper');


//  Factory method implementation
class EvilJoke {
  getJoke(jokeNumber) {
    const jokes = [
      `Журналист спрашивает гонщика—победителя: — Как Вам удалось обойти столь опытных 
      и именитых соперников? — Да просто тормоза отказали...`,
      `Профессиональный гонщик — приятелю: — У меня сломаны два ребра, разбита коленная 
      чашечка и растянуты связки. — Да, профессия гонщика не самая спокойная. 
      — Причем тут моя профессия?! Я сейчас в отпуске, вот жена и потащила по магазинам, 
      где объявили дешевую распродажу.`,
    ];

    return jokes[jokeNumber];
  }
}

class KindJoke {
  getJoke(jokeNumber) {
    const jokes = [
      `В поселке Выпивохино началась очередная гонка по формуле C2H5OH. Лучшие гонщики уже 
      гонят вовсю и находятся на втором этапе перегонки.`,
      'Вчера лежачий полицейский догнал эстонского гонщика.',
    ];

    return jokes[jokeNumber];
  }
}

class Fact {
  getJoke(jokeNumber) {
    const jokes = [
      `На питстопе формулы 1 скорость заправки составляет 12 литров топлива в секунду.
       Они используют оборудование для заправки вертолетов.`,
      `Без аэродинамического спойлера управление теряется на скорости 180 км/ч, средняя 
      скорость гоночного болида более 300 км/ч.`,
    ];

    return jokes[jokeNumber];
  }
}

class MessageGenerator {
  constructor(socket) {
    console.log('MessageGenerator');
    this.socket = socket;
  }

  getTypeOfMessage() {
    const randomNumberOfMessage = geRandomNumber(0)(1);

    const typeOfMessage = this.generateJoke();
    const message = typeOfMessage.getJoke(randomNumberOfMessage);

    this.socket.emit('getRandomMessage', { message });
  }
}

class KindJokeGenerator extends MessageGenerator {
  generateJoke() {
    return new KindJoke();
  }
}

class EvilJokeGenerator extends MessageGenerator {
  generateJoke() {
    return new EvilJoke();
  }
}

class FactGenerator extends MessageGenerator {
  generateJoke() {
    return new Fact();
  }
}

module.exports = {
  KindJokeGenerator,
  EvilJokeGenerator,
  FactGenerator,
};
