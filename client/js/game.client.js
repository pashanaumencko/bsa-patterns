/* eslint-disable no-trailing-spaces */
/* eslint-disable no-param-reassign */
/* eslint-disable no-plusplus */
/* eslint-disable object-curly-newline */
/* eslint-disable no-inner-declarations */
/* eslint-disable prefer-template */
/* eslint-disable no-useless-concat */
/* eslint-disable no-undef */
/* eslint-disable no-use-before-define */
/* eslint-disable no-console */
/* eslint-disable no-restricted-globals */
window.onload = () => {
  const jwt = localStorage.getItem('jwt');
  if (!jwt) {
    location.replace('/login');
  } else {
    const gameContainerElement = document.getElementById('gameContainer');
    let symbolCounter = 0;
    let theGameTimer = null;
    let waitGameTimer = null;
    let botJokingTimer = null;
    let informAboutLeadersTimer = null;
    const socket = io.connect('http://localhost:3000');
    // eslint-disable-next-line no-console
    console.log('connected');
    socket.emit('joinGameAttempt', { token: jwt });

    socket.on('startTimerBeforeTheGame', (payload) => {
      const timeRemaining = moment(payload.startTime).diff(moment(), 'second');

      if (timeRemaining < 0) {
        socket.emit('joinWaitRoom', { token: jwt });
      } else {
        const timerElement = renderTimer(payload.message, timeRemaining);
        gameContainerElement.prepend(timerElement);

        socket.emit('joinGame', { token: jwt });

        reduceBeforeTheGameTimer({
          message: 'The game starts in',
          login: payload.login,
          gameDuration: payload.gameDuration,
          timeRemaining,
        });
      }
    });

    socket.on('greetingsMesssage', (payload) => {
      console.log('greetingsMesssage');
      const botElement = renderBotComments(payload.message);
      gameContainerElement.appendChild(botElement);
    });

    socket.on('getGameDurationTime', (payload) => {
      const timeRemaining = moment(payload.gameDuration).diff(moment(), 'second');

      console.log(timeRemaining);

      const timerElement = renderTimer(payload.message, timeRemaining);
      gameContainerElement.prepend(timerElement);

      reduceWaitGameTimer({
        message: payload.message,
        timeRemaining,
      });
    });

    socket.on('sendTextId', (payload) => {
      const { textId } = payload;
      fetch(`/text/${textId}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${jwt}`,
        },
      }).then((res) => {
        res.json().then((body) => {
          // eslint-disable-next-line prefer-destructuring
          const { text } = body;
          const textElement = renderText(text);
          gameContainerElement.prepend(textElement);
          socket.emit('getTextLength', { token: jwt, textLength: text.length });
          console.log('get text' + ' ' + text);
        });
      }).catch((err) => {
        console.log(err);
        console.log('request went wrong');
      });
    });

    socket.on('joinNewPlayer', (payload) => {
      console.log(`I know that new player with login ${payload.userProgress.login} has joined`);
      const playersContainerElement = document.getElementById('players');

      const updatedPlayersContainerElement = createElement({
        tagName: 'div',
        className: 'players',
        attributes: {
          id: 'players',
        },
      });

      Object.keys(payload.userProgress).forEach((user) => {
        const playerBar = renderPlayerBar({
          playerName: user,
          progress: payload.userProgress[user].progress,
          textLength: payload.userProgress[user].textLength,
        });
        updatedPlayersContainerElement.appendChild(playerBar);
      });

      if (playersContainerElement) {
        playersContainerElement.replaceWith(updatedPlayersContainerElement);
      } else {
        gameContainerElement.appendChild(updatedPlayersContainerElement);
      }
    });

    function renderBotComments(message) {
      const botElement = createElement({
        tagName: 'div',
        className: 'bot',
        attributes: {
          id: 'bot',
        },
      });
      botElement.innerHTML = message;

      return botElement;
    }

    function changeBotComments(message) {
      const botElement = document.getElementById('bot');
      botElement.innerHTML = message;
    }

    function renderPlayerBar({ playerName, progress, textLength }) {
      const playerElement = createElement({ tagName: 'div', className: 'player' });
      const playerNameElement = createElement({ tagName: 'span', className: 'player-name' });
      const playerProgressElement = createElement({
        tagName: 'progress',
        className: 'player-progress',
        attributes: {
          max: textLength,
          value: progress,
          id: `${playerName}Progress`,
        },
      });
      playerElement.innerHTML = playerName;
      playerElement.append(playerNameElement, playerProgressElement);

      return playerElement;
    }

    function reduceBeforeTheGameTimer({ message, timeRemaining, gameDuration, login }) {
      const timerElement = document.getElementById('timer');

      const beforeTheGameTimer = setInterval(() => {
        timerElement.innerHTML = `${message}: ${timeRemaining}s`;
        console.log('tik before the game ' + timeRemaining);
        timeRemaining--;
        if (timeRemaining < 0) {
          clearInterval(beforeTheGameTimer);
          startGame('The game ends in', gameDuration, login);
        }
      }, 1000);
    }

    function clearPreviousGame() {
      const resultListElement = document.getElementById('result');
      console.log(resultListElement);
      if (resultListElement) {
        resultListElement.parentElement.removeChild(resultListElement);
        console.log('Remove results');
      }
    }

    function startGame(message, timeRemaining, login) {
      console.log('Game started');
      clearPreviousGame();

      const timerElement = document.getElementById('timer');
      timerElement.innerHTML = `${message}: ${timeRemaining}s`;
      window.addEventListener('keypress', event => enterSymbolHandler(event, login));
      socket.emit('startGame');
      informAboutLeadersTimer = setInterval(() => socket.emit('checkLeaders'), 15000);
      botJokingTimer = setInterval(() => socket.emit('waitForRandomMessage'), 9000);
      
      reduceTheGameTimer({
        message,
        timeRemaining,
      });
    }

    socket.on('introducePlayers', payload => changeBotComments(payload.message));
    socket.on('informAboutLeaders', payload => changeBotComments(payload.message));
    socket.on('getRandomMessage', payload => changeBotComments(payload.message));
    socket.on('informAboutPlayerFinishing', payload => changeBotComments(payload.message));
    socket.on('informAboutAllPlayersFinishing', payload => changeBotComments(payload.message));

    function reduceWaitGameTimer({ message, timeRemaining }) {
      const timerElement = document.getElementById('timer');
      waitGameTimer = setInterval(() => {
        timerElement.innerHTML = `${message}: ${timeRemaining}s`;
        console.log('wait tik ' + timeRemaining);
        timeRemaining--;
        if (timeRemaining < 0) {
          clearInterval(waitGameTimer);
          clearInterval(botJokingTimer);
          clearInterval(informAboutLeadersTimer);
          socket.emit('joinGameAttempt', { token: jwt });
        }
      }, 1000);
    }

    socket.on('endGame', () => {
      console.log('finish waiting game');
      clearInterval(botJokingTimer);
      clearInterval(informAboutLeadersTimer);
      clearInterval(botJokingTimer);
      while (gameContainerElement.firstChild) {
        gameContainerElement.removeChild(gameContainerElement.firstChild);
      }

      socket.emit('joinGameAttempt', { token: jwt });
    });

    function reduceTheGameTimer({ message, timeRemaining }) {
      const timerElement = document.getElementById('timer');
      theGameTimer = setInterval(() => {
        timerElement.innerHTML = `${message}: ${timeRemaining}s`;
        console.log('tik in the game ' + timeRemaining);
        timeRemaining--;
        if (timeRemaining < 0) {
          clearInterval(theGameTimer);
          clearInterval(botJokingTimer);
          clearInterval(informAboutLeadersTimer);
          socket.emit('finishTime', { token: jwt });
        }
      }, 1000);
    }

    socket.on('getResult', (payload) => {
      console.log('get result list');
      symbolCounter = 0;
      clearInterval(theGameTimer);
      clearInterval(waitGameTimer);
      clearInterval(botJokingTimer);
      clearInterval(informAboutLeadersTimer);
      while (gameContainerElement.firstChild) {
        gameContainerElement.removeChild(gameContainerElement.firstChild);
      }

      const resultListElement = renderResult(payload.results);
      gameContainerElement.appendChild(resultListElement);

      const botElement = renderBotComments(payload.message);
      gameContainerElement.appendChild(botElement);
      socket.emit('getLastComment');
      setTimeout(() => socket.emit('joinGameAttempt', { token: jwt }), 3000);
      
    });

    socket.on('updateProgress', (payload) => {
      const progressElement = document.getElementById(`${payload.login}Progress`);
      progressElement.value++;
    });

    function renderText(text) {
      const textArray = text.split('');
      const textElement = createElement({
        tagName: 'p',
        className: 'text',
        attributes: {
          id: 'text',
        },
      });
      textArray.forEach((symbol) => {
        const symbolElement = createElement({
          tagName: 'span',
          className: 'symbol',
        });
        symbolElement.innerHTML = symbol;
        textElement.append(symbolElement);
      });

      return textElement;
    }

    function renderTimer(message, timeRemaining) {
      const timerElement = createElement({
        tagName: 'p',
        className: 'timer',
        attributes: {
          id: 'timer',
        },
      });
      timerElement.innerHTML = `${message}: ${timeRemaining}s`;

      return timerElement;
    }


    function renderResult(score) {
      const resultLabelElement = createElement({ tagName: 'h3', className: 'result-label' });
      resultLabelElement.innerHTML = 'Final score';
      const resultListElement = createElement({
        tagName: 'ol',
        className: 'result',
        attributes: {
          id: 'result',
        },
      });

      score.forEach((player) => {
        const resultListItemElement = createElement({ tagName: 'li', className: 'result-item' });
        resultListItemElement.innerHTML = player;
        resultListElement.appendChild(resultListItemElement);
      });

      resultListElement.prepend(resultLabelElement);
      return resultListElement;
    }

    function enterSymbolHandler(event, playerName) {
      const progressElement = document.getElementById(`${playerName}Progress`);
      const textElement = document.getElementById('text');

      if (event.which !== 0 && event.charCode !== 0) {
        const symbolArray = [...textElement.children];
        const enteredSymbol = String.fromCharCode(event.which);
        console.log(enteredSymbol);
        if (enteredSymbol === symbolArray[symbolCounter].innerHTML) {
          progressElement.value++;
          symbolArray[symbolCounter].classList.add('entered');
          socket.emit('enteredSymbol', { token: jwt });
          symbolCounter++;
        }
      }
    }

    function createElement({ tagName, className = '', attributes = {} }) {
      const element = document.createElement(tagName);
      element.classList.add(className);
      Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

      return element;
    }
  }
};
