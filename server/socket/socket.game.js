/* eslint-disable prefer-template */
/* eslint-disable no-useless-concat */
/* eslint-disable object-curly-newline */
/* eslint-disable no-plusplus */
/* eslint-disable no-param-reassign */
/* eslint-disable no-console */
const jwt = require('jsonwebtoken');
const moment = require('moment');
const Bot = require('./game.bot');
const LaunchBot = require('./game.bot.launch');
const { userProgress, textId } = require('./globals');
let { initiatedAt, isInitiated, results } = require('./globals');

// eslint-disable-next-line object-curly-newline
function gameSocket({ io, socket }) {
  const botInstance = new LaunchBot(new Bot(io, socket));

  function initiateGame() {
    if (socket.room !== undefined) {
      console.log(`left the room ${socket.room}`);
      socket.leave(socket.room);
    }
    if (!isInitiated) {
      initiatedAt = moment().add(10, 'second');
      isInitiated = true;
      console.log('set startTime');
    }
  }

  socket.on('joinGameAttempt', (payload) => {
    console.log('connected user');
    initiateGame();
    const gameDuration = 60;
    const message = 'The game starts in';
    const user = jwt.verify(payload.token, 'someSecret');
    if (user) {
      socket.emit('startTimerBeforeTheGame', {
        login: user.login,
        startTime: initiatedAt,
        message,
        gameDuration,
      });
      console.log('initiate');
    } else {
      console.log('not verified');
    }
  });

  socket.on('joinGame', (payload) => {
    const user = jwt.verify(payload.token, 'someSecret');
    if (user) {
      socket.emit('sendTextId', { textId });

      console.log('send text id');
    } else {
      console.log('not verified');
    }
  });

  socket.on('getTextLength', (payload) => {
    const user = jwt.verify(payload.token, 'someSecret');
    if (user) {
      userProgress[user.login] = {
        progress: 0,
        textLength: payload.textLength,
      };

      socket.join('gameRoom');
      socket.room = 'gameRoom';
      io.sockets.in('gameRoom').emit('joinNewPlayer', { userProgress });
      botInstance.initiateBot();
      console.log('Join to the game group');
    } else {
      console.log('not verified');
    }
  });

  socket.on('joinWaitRoom', (payload) => {
    const user = jwt.verify(payload.token, 'someSecret');
    if (user) {
      socket.join('waitRoom');
      socket.room = 'waitRoom';
      io.sockets.in('waitRoom').emit('getGameDurationTime', {
        message: 'Game has already started. You will be able to join the game in',
        gameDuration: initiatedAt.add(62, 'second'),
      });
      console.log('Join to the wait group');
    } else {
      console.log('not verified');
    }
  });

  socket.on('startGame', () => botInstance.introducePlayers(userProgress));

  socket.on('checkLeaders', () => botInstance.informAboutLeaders(userProgress));

  socket.on('waitForRandomMessage', () => botInstance.sayRandomMessage());

  socket.on('getLastComment', () => botInstance.informAboutAllPlayersFinishing());

  function allPlayersFinished() {
    const isGameFinish = Object.keys(userProgress)
      .every(player => userProgress[player].progress === userProgress[player].textLength);


    if (isGameFinish) {
      isInitiated = false;
      io.sockets.in('gameRoom').emit('getResult', { results });
      io.sockets.in('waitRoom').emit('endGame');
    }
  }

  socket.on('enteredSymbol', (payload) => {
    const user = jwt.verify(payload.token, 'someSecret');
    if (user) {
      console.log('Updated progress');
      socket.broadcast.to('gameRoom').emit('updateProgress', { login: user.login });
      userProgress[user.login].progress++;
      botInstance.informAboutPlayerFinishing(user.login, userProgress);
      results = Object.keys(userProgress)
        .sort((player, anotherPlayer) => {
          const { progress: playerProgress } = userProgress[player];
          const { progress: anotherPlayerProgress } = userProgress[anotherPlayer];
          return anotherPlayerProgress - playerProgress;
        });
      allPlayersFinished();
      console.log(results);
    } else {
      console.log('not verified');
    }
  });

  socket.on('finishTime', (payload) => {
    console.log('finish time');
    const user = jwt.verify(payload.token, 'someSecret');
    if (user) {
      isInitiated = false;
      io.sockets.in('gameRoom').emit('getResult', { results });
      io.sockets.in('waitRoom').emit('endGame');
    } else {
      console.log('not verified');
    }
  });
}

module.exports = gameSocket;
